import firebase from "firebase/app";
import "firebase/firestore";

const config = {
  apiKey: "AIzaSyBJkuM8Xw_4zOvU4MAAqp5CPQv_ALa-sEQ",
  authDomain: "automaton-quiz.firebaseapp.com",
  databaseURL: "https://automaton-quiz.firebaseio.com",
  projectId: "automaton-quiz",
  storageBucket: "automaton-quiz.appspot.com",
  messagingSenderId: "1049174598393",
  appId: "1:1049174598393:web:66bcb638d43aa35f"
};

const firestoreApp = firebase.initializeApp(config);

const firestore = firestoreApp.firestore();

export default firestore;
